// ---------------------------------------------------------------------------
//  index.js
// ---------------------------------------------------------------------------

const isBoolean = require('@amphibian/is-boolean');
const isNumber = require('@amphibian/is-number');
const isString = require('@amphibian/is-string');
const isObject = require('@amphibian/is-object');
const objectKeys = require('@amphibian/object-keys');
const objectHasProperty = require('@amphibian/object-has-property');
const iterateUpArray = require('@amphibian/iterate-up-array');
const forOwn = require('@amphibian/for-own');
const errors = require('@amphibian/errors');

/**
 * Check if input is not false
 * @param {boolean} boolean
 * @param {boolean} isNotFalse
**/
function notFalse(boolean) {
    return boolean !== false;
}

/**
 * Initialize cache container
 * @param {object} cacheOptions
 * @param {boolean} cacheOptions.enabled
 *
 * @returns {string} cacheContainer
**/
function createCache(cacheOptions = {}) {
    if (!isObject(cacheOptions)) {
        throw errors.typeError(undefined, 'Object', 'cacheOptions');
    }

    if (objectHasProperty(cacheOptions, 'enabled')
    && (!isBoolean(cacheOptions.enabled))) {
        throw errors.typeError(undefined, 'Boolean', 'cacheOptions.enabled');
    }

    const contents = {};

    return {
        contents() {
            const object = {};

            forOwn(contents, (key, cache) => {
                object[key] = {
                    tag: cache.tag,
                    value: cache.value
                };
            });

            return object;
        },
        open(key) {
            if (!key) {
                throw errors.missingRequiredParameters(undefined, 'key');
            } else if (!isString(key)) {
                throw errors.typeError(undefined, 'String', 'key');
            }

            const cache = {};

            cache.set = this.set.bind(this, key);
            cache.get = this.get.bind(this, key);
            cache.invalidate = this.invalidate.bind(this, key);
            cache.fresh = this.fresh.bind(this, key);
            cache.tag = this.tag.bind(this, key);

            return cache;
        },
        set(key, value, options = {}) {
            if (!key) {
                throw errors.missingRequiredParameters(undefined, 'key');
            } else if (!isString(key)) {
                throw errors.typeError(undefined, 'String', 'key');
            }

            if (!isObject(options)) {
                throw errors.typeError(undefined, 'Object', 'options');
            } else if (options.lifetime && !isNumber(options.lifetime)) {
                throw errors.typeError(undefined, 'Number', 'options.lifetime');
            } else if (options.tag && !isString(options.tag)) {
                throw errors.typeError(undefined, 'String', 'options.tag');
            }

            try {
                this.invalidate(key);
            } catch (error) {
                // ... do nothing
            }

            contents[key] = {
                __timeout__: undefined,

                value,
                lifetime: options.lifetime || undefined,
                tag: options.tag || undefined,
                fresh: true
            };

            if (options.lifetime) {
                contents[key].__timeout__ = setTimeout(() => {
                    try {
                        this.invalidate(key);
                    } catch (error) {
                        // ... do nothing
                    }
                }, options.lifetime);
            }
        },
        get(key) {
            if (!key) {
                throw errors.missingRequiredParameters(undefined, 'key');
            } else if (!isString(key)) {
                throw errors.typeError(undefined, 'String', 'key');
            }

            if (notFalse(cacheOptions.enabled)
            && (objectHasProperty(contents, key))) {
                return contents[key].value;
            }

            return undefined;
        },
        invalidate(key) {
            if (key) {
                if (!isString(key)) {
                    throw errors.typeError(undefined, 'String', 'key');
                } else if (!objectHasProperty(contents, key)) {
                    throw errors.invalidInput('unknown_cache_key', 'key', key);
                }

                const cacheKey = contents[key];

                cacheKey.value = undefined;
                cacheKey.fresh = false;

                if (cacheKey.__timeout__) {
                    clearTimeout(cacheKey.__timeout__);
                }

                delete contents[key];
            } else {
                iterateUpArray(objectKeys(contents), (contentKey) => {
                    this.invalidate(contentKey, false);
                });
            }
        },
        fresh(key) {
            if (!key) {
                throw errors.missingRequiredParameters(undefined, 'key');
            } else if (!isString(key)) {
                throw errors.typeError(undefined, 'String', 'key');
            }

            if (notFalse(cacheOptions.enabled)
            && (objectHasProperty(contents, key))) {
                return contents[key].fresh;
            }

            return undefined;
        },
        tag(key) {
            if (!key) {
                throw errors.missingRequiredParameters(undefined, 'key');
            } else if (!isString(key)) {
                throw errors.typeError(undefined, 'String', 'key');
            }

            if (objectHasProperty(contents, key)) {
                return contents[key].tag;
            }

            return undefined;
        }
    };
}

module.exports = createCache;
