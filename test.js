const createCache = require('./index.js');

describe('cache', () => {
    it('create a cache container', () => {
        createCache();
    });

    it('create a cache container with options object', () => {
        createCache({});
    });

    it('failing: create a cache container with invalid type options', () => {
        try {
            createCache([]);
        } catch (error) {
            expect(error.code).toBe('type_error');
            expect(error.data[0]).toBe('Object');
            expect(error.data[1]).toBe('cacheOptions');

            return;
        }

        throw new Error('No error thrown.');
    });

    it('failing: create a cache container with invalid type options.enabled', () => {
        try {
            createCache({enabled: 'something'});
        } catch (error) {
            expect(error.code).toBe('type_error');
            expect(error.data[0]).toBe('Boolean');
            expect(error.data[1]).toBe('cacheOptions.enabled');

            return;
        }

        throw new Error('No error thrown.');
    });

    it('set cache number', () => {
        const cache = createCache();
        cache.set('key', 1234);
    });

    it('set cache string', () => {
        const cache = createCache();
        cache.set('key', 'value');
    });

    it('set cache array', () => {
        const cache = createCache();
        cache.set('key', [1, 2, 3, 4]);
    });

    it('set cache object', () => {
        const cache = createCache();
        cache.set('key', {my_value: 'test'});
    });

    it('get complete cache contents', () => {
        const cache = createCache();
        cache.set('key', {my_value: 'test'});

        expect(cache.contents()).toEqual({
            key: {
                tag: undefined,
                value: {my_value: 'test'}
            }
        });
    });

    it('failing: set cache with no key', () => {
        const cache = createCache();

        try {
            cache.set();
        } catch (error) {
            expect(error.code).toBe('missing_required_parameters');
            expect(error.data[0]).toBe('key');

            return;
        }

        throw new Error('No error thrown.');
    });

    it('failing: set cache with invalid type key', () => {
        const cache = createCache();

        try {
            cache.set([]);
        } catch (error) {
            expect(error.code).toBe('type_error');
            expect(error.data[0]).toBe('String');
            expect(error.data[1]).toBe('key');

            return;
        }

        throw new Error('No error thrown.');
    });

    it('failing: set cache with invalid type options', () => {
        const cache = createCache();

        try {
            cache.set('key', 'value', []);
        } catch (error) {
            expect(error.code).toBe('type_error');
            expect(error.data[0]).toBe('Object');
            expect(error.data[1]).toBe('options');

            return;
        }

        throw new Error('No error thrown.');
    });

    it('failing: set cache with invalid type options.lifetime', () => {
        const cache = createCache();

        try {
            cache.set('key', 'value', {lifetime: []});
        } catch (error) {
            expect(error.code).toBe('type_error');
            expect(error.data[0]).toBe('Number');
            expect(error.data[1]).toBe('options.lifetime');

            return;
        }

        throw new Error('No error thrown.');
    });

    it('failing: set cache with invalid type options.tag', () => {
        const cache = createCache();

        try {
            cache.set('key', 'value', {tag: []});
        } catch (error) {
            expect(error.code).toBe('type_error');
            expect(error.data[0]).toBe('String');
            expect(error.data[1]).toBe('options.tag');

            return;
        }

        throw new Error('No error thrown.');
    });

    it('get cache', () => {
        const cache = createCache();
        const value = 'some-value';

        cache.set('key', value);
        expect(cache.get('key')).toBe(value);
    });

    it('get cache from undefined key', () => {
        const cache = createCache();
        expect(cache.get('key')).toBe(undefined);
    });

    it('get undefined when cache is disabled', () => {
        const cache = createCache({enabled: false});
        expect(cache.get('key')).toBe(undefined);
    });

    it('failing: get cache with no key', () => {
        const cache = createCache();

        try {
            cache.get();
        } catch (error) {
            expect(error.code).toBe('missing_required_parameters');
            expect(error.data[0]).toBe('key');

            return;
        }

        throw new Error('No error thrown.');
    });

    it('failing: get cache with invalid type key', () => {
        const cache = createCache();

        try {
            cache.get([]);
        } catch (error) {
            expect(error.code).toBe('type_error');
            expect(error.data[0]).toBe('String');
            expect(error.data[1]).toBe('key');

            return;
        }

        throw new Error('No error thrown.');
    });

    it('invalidate single cache key and assert freshness', () => {
        const cache = createCache();

        cache.set('key', 'value');
        cache.invalidate('key');

        expect(cache.fresh('key')).toBe(undefined);
    });

    it('failing: invalidate single cache with invalid type key', () => {
        const cache = createCache();

        try {
            cache.invalidate([]);
        } catch (error) {
            expect(error.code).toBe('type_error');
            expect(error.data[0]).toBe('String');
            expect(error.data[1]).toBe('key');

            return;
        }

        throw new Error('No error thrown.');
    });

    it('failing: invalidate single cache for unknown key', () => {
        const cache = createCache();
        const undefinedKey = 'undefined-key';

        try {
            cache.invalidate(undefinedKey);
        } catch (error) {
            expect(error.code).toBe('unknown_cache_key');
            expect(error.data[0]).toBe('key');
            expect(error.data[1]).toBe(undefinedKey);

            return;
        }

        throw new Error('No error thrown.');
    });

    it('invalidate single cache key and get contents', () => {
        const cache = createCache();

        cache.set('key', 'value');
        cache.invalidate('key');

        expect(cache.get('key')).toBe(undefined);
    });

    it('invalidate cache and assert freshness', () => {
        const cache = createCache();

        cache.set('key', 'value');
        cache.set('other-key', 'value');
        cache.invalidate();

        expect(cache.fresh('key')).toBe(undefined);
        expect(cache.fresh('other-key')).toBe(undefined);
    });

    it('invalidate cache and get contents', () => {
        const cache = createCache();

        cache.set('key', 'value');
        cache.set('other-key', 'value');
        cache.invalidate();

        expect(cache.get('key')).toBe(undefined);
        expect(cache.get('other-key')).toBe(undefined);
    });

    it('assert cache without lifetime freshness', () => {
        const cache = createCache();
        cache.set('key', 'value');
        expect(cache.fresh('key')).toBe(true);
    });

    it('assert cache freshness for unknown key', () => {
        const cache = createCache();
        expect(cache.fresh('key')).toBe(undefined);
    });

    it('assert cache with lifetime freshness', () => {
        const cache = createCache();

        cache.set('key', 'value', {lifetime: 300 * 1000});
        expect(cache.fresh('key')).toBe(true);
    });

    it('assert cache freshness when cache is disabled', () => {
        const cache = createCache({enabled: false});
        cache.set('key', 'value');
        expect(cache.fresh('key')).toBe(undefined);
    });

    it('assert cache with lifetime freshness after expiration', () => {
        const cache = createCache();

        cache.set('key', 'value', {lifetime: 200});

        return new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, 300);
        }).then(() => {
            expect(cache.fresh('key')).toBe(undefined);
        });
    });

    it('failing: assert freshness without key', () => {
        const cache = createCache();

        try {
            cache.fresh();
        } catch (error) {
            expect(error.code).toBe('missing_required_parameters');
            expect(error.data[0]).toBe('key');

            return;
        }

        throw new Error('No error thrown.');
    });

    it('failing: assert freshness with invalid type key', () => {
        const cache = createCache();

        try {
            cache.fresh([]);
        } catch (error) {
            expect(error.code).toBe('type_error');
            expect(error.data[0]).toBe('String');
            expect(error.data[1]).toBe('key');

            return;
        }

        throw new Error('No error thrown.');
    });

    it('set and get cache tag', () => {
        const cache = createCache();
        const tag = 'some-tag';

        cache.set('key', 'value', {tag});
        expect(cache.tag('key')).toBe(tag);
    });

    it('get cache tag for unknown cache', () => {
        const cache = createCache();
        expect(cache.tag('key')).toBe(undefined);
    });

    it('failing: get cache tag without key', () => {
        const cache = createCache();

        try {
            cache.tag();
        } catch (error) {
            expect(error.code).toBe('missing_required_parameters');
            expect(error.data[0]).toBe('key');

            return;
        }

        throw new Error('No error thrown.');
    });

    it('failing: get cache tag with invalid type key', () => {
        const cache = createCache();

        try {
            cache.tag([]);
        } catch (error) {
            expect(error.code).toBe('type_error');
            expect(error.data[0]).toBe('String');
            expect(error.data[1]).toBe('key');

            return;
        }

        throw new Error('No error thrown.');
    });

    it('open a cache', () => {
        const cache = createCache();
        cache.set('key');
        cache.open('key');
    });

    it('failing: get cache tag without key', () => {
        const cache = createCache();

        try {
            cache.open();
        } catch (error) {
            expect(error.code).toBe('missing_required_parameters');
            expect(error.data[0]).toBe('key');

            return;
        }

        throw new Error('No error thrown.');
    });

    it('failing: get cache tag with invalid type key', () => {
        const cache = createCache();

        try {
            cache.open([]);
        } catch (error) {
            expect(error.code).toBe('type_error');
            expect(error.data[0]).toBe('String');
            expect(error.data[1]).toBe('key');

            return;
        }

        throw new Error('No error thrown.');
    });

    it('set cache on opened cache', () => {
        const cache = createCache();
        cache.open('key').set('value');

        expect(cache.get('key')).toBe('value');
    });

    it('get cache on opened cache', () => {
        const cache = createCache();

        cache.set('key', 'value');
        expect(cache.open('key').get()).toBe('value');
    });

    it('invalidate cache on opened cache', () => {
        const cache = createCache();

        cache.set('key', 'value');
        cache.open('key').invalidate();

        expect(cache.open('key').fresh()).toBe(undefined);
    });

    it('assert freshness on opened cache', () => {
        const cache = createCache();

        cache.set('key', 'value');
        expect(cache.open('key').fresh()).toBe(true);
    });

    it('get tag from opened cache', () => {
        const cache = createCache();

        cache.set('key', 'value', {tag: 'tag'});
        expect(cache.open('key').tag()).toBe('tag');
    });
});
